@extends('layouts.frontend.frontend')
@section('content')

<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Blog Grid Style
                    <small>Welcome to Nexa Application</small>
                </h2>
            </div>
            <!-- <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                    <li class="breadcrumb-item"><a href="blog-dashboard.html">Blog</a></li>
                    <li class="breadcrumb-item active">Blog Grid</li>
                </ul>
            </div> -->
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">                    
        @foreach($posts as $row)
            <div class="col-lg-6 col-md-12 left-box">
                <div class="card single-blog-post">
                    <div class="img-holder">
                    @if($row->image_posts)
                        <div class="img-post"><img src="{{asset('storage/'.$row->image_posts)}}" width="100%" alt="Awesome Image"></div>
                    @endif
                        <div class="date-box">{{$row->created_at}}</div>
                    </div>
                    <div class="body">
                        <ul class="meta list-inline">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i>Posted By :{{$row->author_id}}</a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-lime"></i>{{$row->categories}}</a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                        </ul>
                        <h3 class="m-t-20"><a href="blog-details.html">{{$row->title}}</a></h3>
                        {!!$row->content!!}
                        <a href="blog-details.html" class="btn btn-raised btn-default">Read More</a>
                    </div>
                </div>
            </div> 
        @endforeach
        </div>
        <div class="card">
                    <div class="body">
                        <h4 class="m-b-20">COMMENTS</h4>
                        <ul class="comment-reply list-unstyled">
                        @foreach($comments as $data)
                            <li class="row">
                                <div class="icon-box col-md-2 col-4"><img class="img-fluid" src="{{asset('backend/assets/images/sm/avatar2.jpg')}}" alt="Awesome Image"></div>
                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                    <h5 class="m-b-0">{{$data->name}}</h5>
                                    <p>{{$data->content}}</p>
                                </div>
                            </li>
                        @endforeach
                        </ul>                                        
                                <ul class="pagination">
                                    <li class="page-item">{{$comments->links()}}</li>
                                </ul>
                    </div>
                </div>
        <ul class="pagination">
                    <li class="page-item">{{$posts->links()}}</li>
        </ul>
    </div>



@endsection