@extends('layouts.backend.global')
@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>New Post
                    <small>Welcome to Nexa Application</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                    <li class="breadcrumb-item"><a href="blog-dashboard.html">Blog</a></li>
                    <li class="breadcrumb-item active">New Post</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <form action="{{route('posts.store')}}"  method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Blog title" required />
                            </div>
                        </div>
                        <select class="form-control show-tick" name="categories" id="categories" required >
                            <option>Select Category --</option>
                            @foreach($categories as $row)
                                <option value="{{$row->id}}">{{$row->categories}}</option>
                            @endforeach
                        </select>
                            <div class="dz-message">
                                <div class="drag-icon-cph"> <i class="material-icons">touch_app</i> </div>
                                <h3>Drop files here or click to upload.</h3>
                                <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em> </div>
                            <div class="fallback">
                                <input type="file"  name="image_posts" id="image_posts" required >
                            </div>
                        <textarea id="ckeditor" type="text" name="content" id="content" required >

                        </textarea>
                        <button type="submit" class="btn btn-raised btn-primary waves-effect m-t-20">Post</button>
                        </form>
                    </div>
                </div>               
            </div>            
        </div>
    </div>
@endsection