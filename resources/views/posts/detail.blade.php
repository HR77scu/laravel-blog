@extends('layouts.backend.global')
@section('content')


<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Blog Detail
                    <small>Welcome to Nexa Application</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                    <li class="breadcrumb-item"><a href="blog-dashboard.html">Blog</a></li>
                    <li class="breadcrumb-item active">Blog Detail</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 left-box">
                <div class="card single-blog-post">
                    <div class="img-holder">
                        <div class="img-post"><img src="assets/images/blog/blog-page-1.jpg" alt="Awesome Image"></div>
                        <div class="date-box"><span>25</span><br>June</div>
                    </div>
                    <div class="body">
                        <ul class="meta list-inline">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i>Posted By: John Smith</a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-red"></i>Photography</a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                        </ul>
                        <h3 class="m-t-20"><a href="blog-details.html">All photographs are accurate. None of them is the truth</a></h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</p>
                        <p>Lorem Ipsum is sim Lorem Ipsum has been the industry's standard dummy into electronic typesetting, remaining essentially unchanged.</p>
                        <blockquote class="blockquote">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing e um is sim Lorem Ipsum has been the industry's standard dummy into ellit. Integer posuere erat a ante.</p>
                            <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                        </blockquote>                        
                        <p>Lorem Ipsum is sim Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                        <p>Lorem Ipsum is sim Lorem Ipsum has been the industry's standard dummy into <strong>electronic</strong> typesetting, remaining essentially unchanged.</p>
                    </div>                    
                </div>
                <div class="card">
                    <div class="body">
                        <h4 class="m-b-20">COMMENTS 3</h4>
                        <ul class="comment-reply list-unstyled">
                            <li class="row">
                                <div class="icon-box col-md-2 col-4"><img class="img-fluid" src="assets/images/sm/avatar2.jpg" alt="Awesome Image"></div>
                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                    <h5 class="m-b-0">Gigi Hadid </h5>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
                                    <ul class="list-inline">
                                        <li><a href="javascript:void(0);">Feb 09 2017</a></li>
                                        <li><a href="javascript:void(0);">Reply</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="row">
                                <div class="icon-box col-md-2 col-4"><img class="img-fluid" src="assets/images/sm/avatar3.jpg" alt="Awesome Image"></div>
                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                    <h5 class="m-b-0">Christian Louboutin</h5>
                                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scramble</p>
                                    <ul class="list-inline">
                                        <li><a href="javascript:void(0);">Feb 12 2017</a></li>
                                        <li><a href="javascript:void(0);">Reply</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="row">
                                <div class="icon-box col-md-2 col-4"><img class="img-fluid" src="assets/images/sm/avatar4.jpg" alt="Awesome Image"></div>
                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                    <h5 class="m-b-0">Kendall Jenner</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
                                    <ul class="list-inline">
                                        <li><a href="javascript:void(0);">March 20 2017</a></li>
                                        <li><a href="javascript:void(0);">Reply</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>                                        
                    </div>
                </div>
                <div class="card">
                    <div class="body">
                        <div class="comment-form">
                            <h4>LEAVE A REPLY</h4>
                            <p>Your email address will not be published. Required fields are marked *</p>
                            <form action="#" class="row ft-fm-2 mtt40">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Your Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-raised btn-primary">SUBMIT</button>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 right-box">
                <div class="card">
                    <div class="body">
                        <div class="widget search">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Search...">
                                </div>
                                <button class="btn btn-raised btn-primary m-t-10" type="submit">Search</button>
                            </div>
                        </div>                        
                        <div class="widget popular-post">
                            <h2 class="title">Popular Posts</h2>
                            <ul class="list-unstyled">
                                <li class="row">
                                    <div class="icon-box col-4">
                                        <img class="img-fluid" src="assets/images/blog/1.jpg" alt="Awesome Image">
                                    </div>
                                    <div class="text-box col-8 p-l-0">
                                        <h5><a href="javascript:void(0);">Lorem Ipsum is simply dummy text</a></h5>
                                        <span class="time">02 Dec.</span>
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="icon-box col-4"><img class="img-fluid" src="assets/images/blog/2.jpg" alt="Awesome Image"></div>
                                    <div class="text-box col-8 p-l-0">
                                        <h5><a href="javascript:void(0);">Lorem Ipsum is simply dummy text</a></h5>
                                        <span>02 Dec.</span>
                                    </div>
                                </li>
                                <li class="row">
                                    <div class="icon-box col-4"><img class="img-fluid" src="assets/images/blog/3.jpg" alt="Awesome Image"></div>
                                    <div class="text-box col-8 p-l-0">
                                        <h5><a href="javascript:void(0);">Lorem Ipsum is simply dummy text</a></h5>
                                        <span>02 Dec.</span>
                                    </div>
                                </li>
                            </ul>                            
                        </div>                        
                        <div class="widget text-widget">
                            <h2 class="title">Text widget</h2>                            
                                <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h5>                            
                        </div>                        
                        <div class="widget tag-clouds">
                            <h2 class="title">Tag Clouds</h2>
                            <ul class="list-unstyled">
                                <li><a href="javascript:void(0);" class="tag label label-default">Design</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-success">Project</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-info">Creative UX</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-success">Wordpress</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-warning">HTML5</a></li>
                            </ul>                            
                        </div>                        
                        <div class="widget newsletter">
                            <h2 class="title">Email Newsletter</h2>
                            <p>Get our products/news earlier than others, let’s get in touch.</p>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Enter Your Email Address">
                                </div>
                                <button class="btn btn-raised btn-primary m-t-10" type="submit">SUBSCRIBE</button>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection