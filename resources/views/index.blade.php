@extends('layouts.frontend.frontend')
@section('content')
<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Blog List
                    <small>Welcome to Nexa Application</small>
                </h2>
            </div>
            <!-- <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                    <li class="breadcrumb-item"><a href="blog-dashboard.html">Blog</a></li>
                    <li class="breadcrumb-item active">Blog List</li>
                </ul>
            </div> -->
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 left-box">

@foreach($posts as $data)
                <div class="card single-blog-post">
                        <div class="img-holder">
                        @if($data->image_posts)
                            <div class="img-post"><img src="{{asset('storage/'.$data->image_posts)}}" width="100%" alt="Awesome Image"></div>
                        @endif
                            <div class="date-box">{{$data->created_at}}</div>
                        </div>
                        <div class="body">
                            <ul class="meta list-inline">
                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i>Posted By: {{$data->author_id}}</a></li>
                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-green"></i>{{$data->categories}}</a></li>
                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-comment-text col-blue"></i>Comments: 3</a></li>
                            </ul>
                            <h3 class="m-t-20"><a href="blog-details.html">{{$data->title}}</a></h3>
                            <p>{!!$data->content!!}</p>
                            <a href="blog-details.html" class="btn btn-raised btn-default">Read More</a>
                        </div>
                </div>      
@endforeach
                <div class="card">
                    <div class="body">
                        <h4 class="m-b-20">COMMENTS</h4>
                        <ul class="comment-reply list-unstyled">
                        @foreach($comments as $data)
                            <li class="row">
                                <div class="icon-box col-md-2 col-4"><img class="img-fluid" src="{{asset('backend/assets/images/sm/avatar2.jpg')}}" alt="Awesome Image"></div>
                                <div class="text-box col-md-10 col-8 p-l-0 p-r0">
                                    <h5 class="m-b-0">{{$data->name}}</h5>
                                    <p>{{$data->content}}</p>
                                </div>
                            </li>
                        @endforeach
                        </ul>                                        
                                <ul class="pagination">
                                    <li class="page-item">{{$comments->links()}}</li>
                                    <!-- <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                                    <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li> -->
                                </ul>
                    </div>
                </div>

                <ul class="pagination">
                    <li class="page-item">{{$posts->links()}}</li>
                    <!-- <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">Next</a></li> -->
                </ul>                
            </div>
            <div class="col-lg-4 right-box">
                <div class="card">
                    <div class="body">
                        <div class="widget search">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Search...">
                                </div>
                                <button class="btn btn-raised btn-primary m-t-10" type="submit">Search</button>
                            </div>
                        </div>                        
                        <div class="widget popular-post">
                            <h2 class="title">Posts</h2>
                            <ul class="list-unstyled">
                            @foreach($side as $row)
                                <li class="row">
                                    <div class="icon-box col-4">
                                    @if($row->image_posts)
                                        <img class="img-fluid" src="{{asset('storage/'.$row->image_posts)}}" alt="Awesome Image">
                                    @endif
                                    </div>
                                    <div class="text-box col-8 p-l-0">
                                        <h5><a href="javascript:void(0);">{{$row->title}}</a></h5>
                                        <span class="time">{{$row->created_at}}</span>
                                    </div>
                                </li>
                            @endforeach
                            </ul>                            
                        </div>                        
                        <!-- <div class="widget text-widget">
                            <h2 class="title">Text widget</h2>                            
                                <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h5>                            
                        </div>                        
                        <div class="widget tag-clouds">
                            <h2 class="title">Tag Clouds</h2>
                            <ul class="list-unstyled">
                                <li><a href="javascript:void(0);" class="tag label label-default">Design</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-success">Project</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-info">Creative UX</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-success">Wordpress</a></li>
                                <li><a href="javascript:void(0);" class="tag label label-warning">HTML5</a></li>
                            </ul>                            
                        </div>                         -->
                        <div class="widget newsletter">
                            <h2 class="title">Comments</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis consequatur repellat cum mollitia ex, iste quas fugiat eveniet quibusdam totam optio, architecto suscipit aut atque cumque nisi natus accusantium officiis.</p>
                            <div class="form-group">
                                <form action="{{route('comments.store')}}" enctype="multipart/form-data" method="post" >
                                @csrf
                                <div class="form-line">
                                    <input type="email" class="form-control" name="email" placeholder="Enter Your Email Address" required >
                                </div>
                                <div class="form-line">
                                    <input type="content" class="form-control" name="name" id="name" placeholder="Enter Your Name" required >
                                </div>
                                <div class="form-line">
                                    <input type="content" class="form-control" name="content" id="content" placeholder="Enter Your comments" required >
                                </div>
                                <button class="btn btn-raised btn-primary m-t-10" type="submit">SUBSCRIBE</button>
                                </form>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection