<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class UsersModel extends Model
{
    
	public $id;
	public $name;
	public $email;
	public $email_verified_at;
	public $password;
	public $remember_token;
	public $created_at;
	public $updated_at;

	public static $tableName = 'users';
	public static $connection = 'mysql';

	public function posts(){
		return $this->belongsTo('\App\Models\PostsModel');
	}

	// public function b

}