<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class CategoriesModel extends Model
{
	public $id;
	public $categories;
	public $created_at;
	public $updated_at;
	
	public static $tableName = 'categories';
	public static $connection = 'mysql';
}