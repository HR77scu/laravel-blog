<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class CommentsModel extends Model
{
    
	public $id;
	public $author_id;
	public $posts_id;
	public $content;
	public $created_at;
	public $updated_at;

}