<?php
namespace App\Models;

use Crocodic\LaravelModel\Core\Model;

class PostsModel extends Model
{
    
	public $id;
	public $author_id;
	public $categories;
	public $image_posts;
	public $title;
	public $content;
	public $created_at;
	public $updated_at;

	public static $tabelName = 'posts';
	public static $connection = 'mysql';

}