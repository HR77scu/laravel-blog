<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Repositories\Comments;
use App\Models\CommentsModel;
use Auth;

class CommentsService extends Comments
{
    // TODO : Make your own service method
    public static function  saveData(Request $request){
        $data = new CommentsModel();
        // $data->author_id = $request->get('author_id');
        // $data->posts_id = $request->get("posts_id");
        $data->email = $request->get('email');
        $data->name = $request->get('name');
        $data->content = $request->get('content');
        $data->save();
    }

}