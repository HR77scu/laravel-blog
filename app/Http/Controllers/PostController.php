<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PostsModel;
use App\Services\PostsService;
use App\Repositories\Posts;
use App\Models\CategoriesModel;
use App\Repositories\Categories;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['posts'] = Posts::list();
        return view('posts.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = CategoriesModel::latest();
        return view('posts.create',$data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Posts::saveData($request);
        return redirect()->route('posts.index')->with('status','Data berhasil di simmpan');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = PostsModel::findOrFail($id);
        // return view('posts.detail',$data);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['post'] = Posts::findById($id);
        $data['categories'] = CategoriesModel::latest();
        return view('posts.edit',$data);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Posts::updateData($request, $id);
        return redirect()->route('posts.index')->with('status','Data berhasil di update');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posts::deleteData($id);
        return redirect()->route('posts.index')->with('status','Data berhasil di hapus');
    }

    public function list(){
        $data['posts'] = Posts::grid();
        $data['side'] = Posts::side();
        return view('posts.list',$data);
        // return view('posts.list');
        // return view('posts.list');
    }
}
