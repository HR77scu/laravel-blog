<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Posts;
use App\Repositories\Comments;

class FrontController extends Controller
{
    public function index(){
        $data['posts'] = Posts::grid();
        $data['comments'] = Comments::list();
        $data['side'] = Posts::side();
        return view('index',$data);
    }
    public function PostsData(){
        $data['posts'] = Posts::list();
        $data['comments'] = Comments::list();
        return view('frontend.posts',$data);
    }
}
