<?php
namespace App\Repositories;

use App\Models\CommentsModel;

class Comments extends CommentsModel
{
    // TODO : Make your own query methods
    public static function list(int $limit = 100){
        return static::table()->simplePaginate($limit);
    }
    public static function deleteData($id){
        $data = CommentsModel::find($id);
        $data->delete();
    }
    
}