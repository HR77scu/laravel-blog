<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\PostsModel;
use Illuminate\Support\Facades\Storage;

class Posts extends PostsModel
{
    // TODO : Make your own query methods
    // show
    public static function list(int $limit = 10){
        return static::table()->simplePaginate($limit);
    }

    public static function grid(int $limits = 4){
        return static::table()->simplePaginate($limits);
    }
    public static function side(int $limitss = 3){
        return static::table()->simplepaginate($limitss);
    }
    public static function all(){
        // 
    }


    public static function editData($id){
        PostsModel::find($id);
    }
    // save edit delete
    public static function saveData(Request $request){
        // $data = ($id) = ? Pos 
        $data = new PostsModel();
        // $data->author_id = $request->get('author_id');
        $data->author_id = Auth::user()->id;
        $data->categories = $request->get('categories');
        $file = $request->file('image_posts')->store('posts_image', 'public');
        $data->image_posts = $file;
        // if($request->file('image_posts')){
        //     $file = $request->file('image_posts')->store('posts_image','public');
        //     $data->image_posts = $file;
        // };
        $data->title = $request->get('title');
        $data->content = $request->get('content');
        $data->save();
    }
    public static function updateData(Request $request, $id){
        $data = PostsModel::find($id);
        // $data->author_id = Auth::user()->id;
        $data->categories = $request->get('categories');
        // $file = $request->file('image_posts')->store('posts_image','public');
        // $data->image_posts = $file;
        if($data->image_posts && file_exists(storage_path('app/public/'.$data->image_posts))){
            Storage::delete('public/'.$data->image_posts);
        }
        $file = $request->file('images_posts')->store('posts_image','public');
        $data->image_posts = $file;
        $data->title = $request->get('title');
        $data->content = $request->get('content');
        $data->save();
    }
    public static function deleteData($id){
        $data = PostsModel::find($id);
        Storage::delete('public/'.$data->image_posts);
        $data->delete();
    }
}