<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\CategoriesModel;

class Categories extends CategoriesModel
{
    // TODO : Make your own query methods
    public static function list(int $limit = 10){
        return static::table()->simplePaginate($limit);
    }

    public static function savedata(Request $request){
        $data = new CategoriesModel();
        $data->categories = $request->get('categories');
        $data->save();
    }
    public static function deleteData($id){
        $data = CategoriesModel::find($id);
        $data->delete();
    }
    public static function updatedata(Request $request, $id){
        $data = CategoriesModel::find($id);
        $data->categories = $request->get('categories');
        $data->save();
    }
}