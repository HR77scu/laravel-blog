<?php

use Illuminate\Database\Seeder;
use App\Models\CategoriesModel;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new CategoriesModel();
        $data->categories = 'elektronik';
        $data->save();
    }
}
