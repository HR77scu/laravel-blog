<?php

use Illuminate\Database\Seeder;
use App\Models\UsersModel;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new UsersModel();
        $data->name = 'admin';
        $data->email = 'admin@example.com';
        $data->password = hash::make('adminadmin');
        $data->save();
    }
}
