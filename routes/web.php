<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

route::get('/','FrontController@index')->name('/');
route::get('/post','FrontController@postsData')->name('front.posts');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::resource('posts', 'PostController');
route::get('posts','PostController@index')->name('posts.index');
route::get('posts/create','PostController@create')->name('posts.create');
route::post('posts/store','PostController@store')->name('posts.store');
route::get('posts/edit/{id}','PostController@edit');
route::PUT('posts/update{id}','PostController@update');
route::get('posts/list','PostController@list')->name('posts_list');
route::get('posts/delete/{id}','PostController@destroy');

Route::resource('comments','CommentsController');

// Route::resource('categories','CategoriesController');
Route::get('categories','CategoriesController@index')->name('categories.index');
Route::post('categories/store','CategoriesController@store')->name('categories.store');
Route::get('categories/delete/{id}','CategoriesController@destroy');
Route::get('categories/edit{id}','CategoriesController@edit');
Route::PUT('categories/update/{id}','CategoriesController@update');

// route::resource('comments','CommentsController');
Route::get('comments','CommentsController@index')->name('comments.index');
Route::get('comments/destroy{$id}','CommentsController@destroy');
